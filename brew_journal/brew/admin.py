from django.contrib import admin
from .models import Brew, Ingredient
# Register your models here.

admin.site.register(Brew)
admin.site.register(Ingredient)
