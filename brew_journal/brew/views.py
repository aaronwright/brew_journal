from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Brew, Ingredient
from rest_framework import viewsets
from .serializers import BrewSerializer, SearchSerializer
from rest_framework import exceptions
from brew.permissions import IsOwnerOrReadOnly
from rest_framework import permissions
from rest_framework.views import APIView



class BrewViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                      IsOwnerOrReadOnly,)
    queryset = Brew.objects.all()
    serializer_class = BrewSerializer

    def perform_create(self, serializer):
        print('SERIALIZER')
        print(serializer)
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        if self.action == 'list':
            return self.queryset.filter(user=self.request.user)
        return self.queryset

        return Response(status=status.HTTP_201_CREATED)
    
    def update(self, validated_data, pk):
        brew_to_edit = Brew.objects.get(id=pk)
        brew_to_edit.title = validated_data.data['title']
        brew_to_edit.description = validated_data.data['description']
        brew_to_edit.original_gravity = validated_data.data['original_gravity']
        brew_to_edit.final_gravity = validated_data.data['final_gravity']
        brew_to_edit.save()

        ing_ids = []

        ingredients_to_edit = validated_data.data['ingredient_set']

        for ingredient in ingredients_to_edit:
            if('id' in ingredient.keys()):
                ing_ids.append(ingredient['id'])
            else:
                new_ing = Ingredient()
                new_ing.name = ingredient['name']
                new_ing.type = ingredient['type']
                new_ing.measurement_unit = ingredient['measurement_unit']
                new_ing.amount = ingredient['amount']
                new_ing.brew = brew_to_edit 
                new_ing.time = ingredient['time']
                new_ing.save()
                ing_ids.append(new_ing.id)
                
        
        allIngredients = Ingredient.objects.filter(brew=brew_to_edit)
        for ingredient in allIngredients:
            if ingredient.id not in ing_ids:
                #if ID not in array, it must have been deleted 
                print(ingredient.id)
                ingredient.delete()
        return Response(status=status.HTTP_202_ACCEPTED)
    
    def pre_delete(self, obj):
            print('obj: ')
            print(obj)
            if obj:
                raise exceptions.ParseError("Too late to delete")


class SearchList(APIView):
    def get(self, request, format=None):
        brews = Brew.objects.all()
        serializer = BrewSerializer(brews, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = SearchSerializer(data=request.data)
        if(serializer.is_valid()):
            query = serializer.validated_data['query']
            results = Brew.objects.filter(title__icontains=query).all()
            if (results.count() == 0):
                print(results)
                return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)

            resultSerializer = BrewSerializer(results, many=True)
            return Response(resultSerializer.data, status=status.HTTP_200_OK)
        #return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def BrewShow(request, id):
    brew = Brew.objects.get(id=id)
    ingredients = Ingredient.objects.filter(brew=brew)
    return render(request, 'show.html', {'brew': brew, 'ingredients': ingredients})