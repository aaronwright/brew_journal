from django.db import models
from django.contrib.auth.models import User


class Brew(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    final_gravity = models.DecimalField(max_digits=4, decimal_places=3, blank=True, null=True)
    original_gravity = models.DecimalField(max_digits=4, decimal_places=3, blank=True, null=True)
    def __str__(self):
        return self.title



class Ingredient(models.Model):
    ING_TYPE = (
        ('Malt', 'Malt'),
        ('Hop', 'Hop'),
        ('Extract', 'Extract'),
        ('Water', 'Water')
    )
    MEASUREMENT = (
        ('OZ', 'Ounces'),
        ('LB', 'Pounds'),
        ('G', 'Gallons'),
        ('#', 'Number')
    )
    name = models.CharField(max_length=50)
    type = models.CharField(max_length=15, choices=ING_TYPE)
    measurement_unit = models.CharField(max_length=15, choices=MEASUREMENT)
    amount = models.DecimalField(max_digits=5, decimal_places=2)
    brew = models.ForeignKey(Brew, on_delete=models.CASCADE)
    time = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return self.name