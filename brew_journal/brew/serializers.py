from .models import Brew, Ingredient
from rest_framework import serializers

class IngredientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('name', 'type', 'measurement_unit', 'amount', 'time', 'id')



class BrewSerializer(serializers.HyperlinkedModelSerializer):
    ingredient_set = IngredientSerializer(many=True)
    title = serializers.CharField(max_length=50, required=True)
    class Meta:
        model = Brew
        fields = ('title', 'description', 'user_id', 'ingredient_set', 'id', 'original_gravity', 'final_gravity')

    def create(self, validated_data):

        ingredients = validated_data['ingredient_set']
        brew = Brew()

        print('validated data: ')
        print(validated_data)

        brew.title = validated_data['title']
        brew.description = validated_data['description']
        brew.original_gravity = validated_data['original_gravity']
        brew.final_gravity = validated_data['final_gravity']
        brew.user = self.context['request'].user
        brew.save()

        for ingredient in ingredients:
            ing = Ingredient()
            ing.name = ingredient['name']
            ing.type = ingredient['type']
            ing.measurement_unit = ingredient['measurement_unit']
            ing.amount = ingredient['amount']
            ing.brew = brew
            ing.time = ingredient['time']
            ing.save()
        return brew
            

class SearchSerializer(serializers.Serializer):
    query = serializers.CharField(required=True, max_length=50)