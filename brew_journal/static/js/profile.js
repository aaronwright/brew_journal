Vue.use(VueResource);

new Vue({
el: "#starting",
delimiters: ['${','}'],
data: function() {
    return {
        showBeerForm: true,
        editingBrew: false,
        showSearchResults: false,
        brewLink: '/brew/',
        searchError: '',
        searchTerm: '',
        brewErrors: [],
        btnTxt: 'Add A New Beer',
        addBeerBtnTxt: 'Add A Beer',
        editBeerBtnTxt: 'Edit Beer',
        ingredients: [],
        ingredient: {id: null,
                    name: '',
                    amount: null,
                    measurement_unit: '',
                    type: '',
                    time: '',
                    },
        brews: [],
        brew: {
            title: '',
            description: '',
            final_gravity: null,
            original_gravity: null
        },
        searchResults: []
    }
 },
    mounted: function() {
        this.getBrews();
        },
    methods: {
    getBrews: function(e) {
        this.$http.get("/brews").then(response => {
            this.brews = response.body;
            }, error => {
            console.error(error);
            });
    },
    toggleBeerForm: function(e) {
        e.preventDefault();
        this.showBeerForm = true;
    },
    toggleSearchForm: function(e) {
        e.preventDefault();
        this.showBeerForm = false;
    },
    addIngredient: function(e) {
            e.preventDefault();
            var newIngredient = {
                name: this.ingredient.name,
                amount: this.ingredient.amount,
                measurement_unit: this.ingredient.measurement_unit,
                type: this.ingredient.type,
                time: this.ingredient.time
            }
            this.ingredients.push(newIngredient);
            this.ingredient.name = '';
            this.ingredient.amount = 0.0;
            this.ingredient.measurement_unit = '';
            this.ingredient.type = '';
            this.ingredient.time = '';
        },
    getCookie: function(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie !== '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        },
    addNewBrew: function(e) {
        e.preventDefault();
        // validate first
        this.brew.errors = [];
        if (!this.brew.title) {
            this.brewErrors.push('Title cannot be blank');
        }

        if (!this.brew.description) {
            this.brewErrors.push('Beer description cannot be blank.')
        }

        if (this.brewErrors.length > 0) {
            return '';
        }

            var csrf = this.getCookie('csrftoken');
            console.log('GRAVITY: ', this.brew.original_gravity);
            var data = {
                'csrfmiddlewaretoken': csrf,
                title: this.brew.title,
                description: this.brew.description,
                ingredient_set: this.ingredients,
                original_gravity: this.brew.original_gravity,
                final_gravity: this.brew.final_gravity
            }
            Vue.http.interceptors.push((request, next) => {
                request.headers.set('X-CSRFTOKEN', csrf)
                next()
              })
            this.$http.post("brews/", data).then(response => {
                this.getBrews();
                this.ingredients = [];
                this.brew = {
                    title: '',
                    description: '',
                    finalGravity: null,
                    originalGavity: null
                };
                this.brewErrors = [];
                }, error => {
                this.brewErrors.push('Something went wrong with your request. Please try again');
                });
        },
    editBrew: function(id, event) {
        event.preventDefault();
        this.editingBrew = true;
        this.btnTxt = this.editBeerBtnTxt;
        var selected = this.brews.filter(function(brew) {
            return brew.id == id;
        });
        this.brew = selected[0];
        this.ingredients = selected[0].ingredient_set;
    },
    editBrewRequest: function(e) {
        e.preventDefault();
        var csrf = this.getCookie('csrftoken');
        var data = {
            'csrfmiddlewaretoken': csrf,
            title: this.brew.title,
            id: this.brew.id,
            description: this.brew.description,
            ingredient_set: this.ingredients,
            original_gravity: this.brew.original_gravity,
            final_gravity: this.brew.final_gravity
        }
        Vue.http.interceptors.push((request, next) => {
            request.headers.set('X-CSRFTOKEN', csrf)
            next()
          })

        var url = 'brews/' + this.brew.id + '/';
        this.$http.put(url, data).then(response => {
            this.getBrews();
            this.ingredients = [];
            this.brew = {
                title: '',
                description: '',
                finalGravity: null,
                originalGavity: null
            };
            this.editingBrew = false;
            this.btnTxt = this.addBeerBtnTxt;
            this.brewErrors = [];
            }, error => {
            console.error(error);
            this.brewErrors.push('Something went wrong with your request. Please try again');

            });
    },
    cancelEdit: function(e) {
        e.preventDefault();
        this.brew = {
            title: '',
            description: '',
            finalGravity: null,
            originalGavity: null
        }
        this.ingredients = [];
        this.editingBrew = false;
    },
    removeIngredient: function(id, event) {
        event.preventDefault();
        console.log(id);
        var selected = this.ingredients.filter(function(ing) {
            return ing.id !== id;
        });
        this.ingredients = selected;
        },
    deleteBrew: function(id, event) {
        event.preventDefault();
        var csrf = this.getCookie('csrftoken');
        console.log(csrf);
        var data = {
            'csrfmiddlewaretoken': csrf,
            title: this.brew.title,
            description: this.brew.description,
            ingredient_set: this.ingredients
        }
        Vue.http.interceptors.push((request, next) => {
            request.headers.set('X-CSRFTOKEN', csrf)
            next()
          })

        var url = 'brews/' + id + '/';
            // var url = 'brews/5/';
          this.$http.delete(url, data).then(response => {

              this.getBrews();
              }, error => {
              console.error(error);
              });
    },
    getSearchResults: function(e) {
        e.preventDefault();
        if(this.searchTerm == '') {
            this.searchError = 'Please enter a query into the search form.'
            this.searchResults = [];
        }
        var csrf = this.getCookie('csrftoken');
        var data = {
            'csrfmiddlewaretoken': csrf,
            query: this.searchTerm
        }
        Vue.http.interceptors.push((request, next) => {
            request.headers.set('X-CSRFTOKEN', csrf)
            next()
          })
        this.$http.post("search/", data).then(response => {     
            this.searchResults = response.data;
            this.searchError = '';
            this.showSearchResults = true;
            this.brewErrors = [];

            }, error => {
            console.error(error);
            if (error.status == 404) {
                this.searchError = 'Sorry, we could not find any results for that query. Try another!';
                this.searchResults = [];
            }
            });
    }
    }
  });