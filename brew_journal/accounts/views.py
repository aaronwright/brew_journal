from django.shortcuts import render
from brew.models import Brew
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def UserProfile(request):
    brews = Brew.objects.filter(user=request.user)
    for brew in brews:
        brew.ingredient_set.all()
    return render(request, 'accounts/profile.html', {'brews': brews})